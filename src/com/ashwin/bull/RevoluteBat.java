//
//package  com.ashwin.bull;
//
//import org.anddev.andengine.engine.Engine;
//import org.anddev.andengine.entity.primitive.Line;
//import org.anddev.andengine.entity.primitive.Rectangle;
//import org.anddev.andengine.entity.scene.Scene;
//import org.anddev.andengine.entity.scene.Scene.IOnSceneTouchListener;
//import org.anddev.andengine.entity.scene.background.ColorBackground;
//import org.anddev.andengine.entity.shape.Shape;
//import org.anddev.andengine.entity.sprite.AnimatedSprite;
//import org.anddev.andengine.entity.util.FPSLogger;
//import org.anddev.andengine.extension.physics.box2d.PhysicsConnector;
//import org.anddev.andengine.extension.physics.box2d.PhysicsFactory;
//import org.anddev.andengine.extension.physics.box2d.PhysicsWorld;
//import org.anddev.andengine.extension.physics.box2d.util.constants.PhysicsConstants;
//import org.anddev.andengine.input.touch.TouchEvent;
//import org.anddev.andengine.sensor.accelerometer.AccelerometerData;
//import org.anddev.andengine.sensor.accelerometer.IAccelerometerListener;
//import org.anddev.andengine.ui.IGameInterface;
//import org.anddev.andengine.ui.activity.BaseActivity;
//
//import android.hardware.SensorManager;
//import android.widget.Toast;
//
//import com.badlogic.gdx.math.Vector2;
//import com.badlogic.gdx.physics.box2d.Body;
//import com.badlogic.gdx.physics.box2d.FixtureDef;
//import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
//import com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef;
//
//
//
//public class RevoluteBat extends BaseActivity implements IGameInterface, IAccelerometerListener, IOnSceneTouchListener /*extends BasePhysicsJointExample*/ {
//	// ===========================================================
//	// Constants
//	// ===========================================================
//
//	// ===========================================================
//	// Fields
//	// ===========================================================
//
//	// ===========================================================
//	// Constructors
//	// ===========================================================
//
//	// ===========================================================
//	// Getter & Setter
//	// ===========================================================
//
//	// ===========================================================
//	// Methods for/from SuperClass/Interfaces
//	// ===========================================================
//	protected Engine mEngine;
//	protected PhysicsWorld mPhysicsWorld;
//	protected static final int CAMERA_WIDTH = 720;
//	protected static final int CAMERA_HEIGHT = 480;
//	@Override
//	public Scene onLoadScene() {
//		this.mEngine.registerUpdateHandler(new FPSLogger());
//
//		final Scene scene = new Scene(2);
//		scene.setBackground(new ColorBackground(0, 0, 0));
//		scene.setOnSceneTouchListener(this);
//
//		this.mPhysicsWorld = new PhysicsWorld(new Vector2(0, SensorManager.GRAVITY_EARTH), false);
//
//		final Shape ground = new Rectangle(0, CAMERA_HEIGHT - 2, CAMERA_WIDTH, 2);
//		final Shape roof = new Rectangle(0, 0, CAMERA_WIDTH, 2);
//		final Shape left = new Rectangle(0, 0, 2, CAMERA_HEIGHT);
//		final Shape right = new Rectangle(CAMERA_WIDTH - 2, 0, 2, CAMERA_HEIGHT);
//
//		final FixtureDef wallFixtureDef = PhysicsFactory.createFixtureDef(0, 0.5f, 0.5f);
//		PhysicsFactory.createBoxBody(this.mPhysicsWorld, ground, BodyType.StaticBody, wallFixtureDef);
//		PhysicsFactory.createBoxBody(this.mPhysicsWorld, roof, BodyType.StaticBody, wallFixtureDef);
//		PhysicsFactory.createBoxBody(this.mPhysicsWorld, left, BodyType.StaticBody, wallFixtureDef);
//		PhysicsFactory.createBoxBody(this.mPhysicsWorld, right, BodyType.StaticBody, wallFixtureDef);
//
//		scene.getBottomLayer().addEntity(ground);
//		scene.getBottomLayer().addEntity(roof);
//		scene.getBottomLayer().addEntity(left);
//		scene.getBottomLayer().addEntity(right);
//
//		scene.registerUpdateHandler(this.mPhysicsWorld);
//
//		this.initJoints(scene);
//		Toast.makeText(this, "In this example, the revolute joints have their motor enabled.", Toast.LENGTH_LONG).show();
//		return scene;
//	}
//	
//
//	// ===========================================================
//	// Methods
//	// ===========================================================
//
//	private void initJoints(final Scene pScene) {
//		final int centerX = CAMERA_WIDTH / 2;
//		final int centerY = CAMERA_HEIGHT / 2;
//
//		final int spriteWidth = this.mBoxFaceTextureRegion.getTileWidth();
//		final int spriteHeight = this.mBoxFaceTextureRegion.getTileHeight();
//
//		final FixtureDef objectFixtureDef = PhysicsFactory.createFixtureDef(10, 0.2f, 0.5f);
//
//		for(int i = 0; i < 3; i++) {
//			final float anchorFaceX = centerX - spriteWidth * 0.5f + 220 * (i - 1);
//			final float anchorFaceY = centerY - spriteHeight * 0.5f;
//
//			final AnimatedSprite anchorFace = new AnimatedSprite(anchorFaceX, anchorFaceY, this.mBoxFaceTextureRegion);
//			final Body anchorBody = PhysicsFactory.createBoxBody(this.mPhysicsWorld, anchorFace, BodyType.StaticBody, objectFixtureDef);
//
//			final AnimatedSprite movingFace = new AnimatedSprite(anchorFaceX, anchorFaceY + 90, this.mCircleFaceTextureRegion);
//			final Body movingBody = PhysicsFactory.createCircleBody(this.mPhysicsWorld, movingFace, BodyType.DynamicBody, objectFixtureDef);
//
//			anchorFace.animate(200);
//			anchorFace.animate(200);
//			anchorFace.setUpdatePhysics(false);
//			movingFace.setUpdatePhysics(false);
//
//			pScene.getTopLayer().addEntity(anchorFace);
//			pScene.getTopLayer().addEntity(movingFace);
//
//			final Line connectionLine = new Line(anchorFaceX + spriteWidth / 2, anchorFaceY + spriteHeight / 2, anchorFaceX + spriteWidth / 2, anchorFaceY + spriteHeight / 2);
//			pScene.getBottomLayer().addEntity(connectionLine);
//			this.mPhysicsWorld.registerPhysicsConnector(new PhysicsConnector(anchorFace, anchorBody, true, true, false, false){
//				@Override
//				public void onUpdate(final float pSecondsElapsed) {
//					super.onUpdate(pSecondsElapsed);
//					final Vector2 movingBodyWorldCenter = movingBody.getWorldCenter();
//					connectionLine.setPosition(connectionLine.getX1(), connectionLine.getY1(), movingBodyWorldCenter.x * PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT, movingBodyWorldCenter.y * PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT);
//				}
//			});
//			this.mPhysicsWorld.registerPhysicsConnector(new PhysicsConnector(movingFace, movingBody, true, true, false, false));
//
//
//			final RevoluteJointDef revoluteJointDef = new RevoluteJointDef();
//			revoluteJointDef.initialize(anchorBody, movingBody, anchorBody.getWorldCenter());
//			revoluteJointDef.enableMotor = true;
//			revoluteJointDef.motorSpeed = 10;
//			revoluteJointDef.maxMotorTorque = 200;
//
//			this.mPhysicsWorld.createJoint(revoluteJointDef);
//		}
//	}
//	@Override
//	public void onGamePaused() {
//		// TODO Auto-generated method stub
//		
//	}
//	@Override
//	public void onGameResumed() {
//		// TODO Auto-generated method stub
//		
//	}
//	@Override
//	public void onLoadComplete() {
//		// TODO Auto-generated method stub
//		
//	}
//	@Override
//	public Engine onLoadEngine() {
//		// TODO Auto-generated method stub
//		return null;
//	}
//	@Override
//	public void onLoadResources() {
//		// TODO Auto-generated method stub
//		
//	}
//	@Override
//	public void onUnloadResources() {
//		// TODO Auto-generated method stub
//		
//	}
//	@Override
//	public void onAccelerometerChanged(AccelerometerData pAccelerometerData) {
//		// TODO Auto-generated method stub
//		
//	}
//	@Override
//	public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {
//		// TODO Auto-generated method stub
//		return false;
//	}
//
//	// ===========================================================
//	// Inner and Anonymous Classes
//	// ===========================================================
//}
