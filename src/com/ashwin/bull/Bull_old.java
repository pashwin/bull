package com.ashwin.bull;
//package com.ashwin.bull;
//
//import java.util.ArrayList;
//
//import javax.microedition.khronos.opengles.GL10;
//
//import org.anddev.andengine.engine.Engine;
//import org.anddev.andengine.engine.camera.BoundCamera;
//import org.anddev.andengine.engine.camera.hud.controls.AnalogOnScreenControl;
//import org.anddev.andengine.engine.camera.hud.controls.BaseOnScreenControl;
//import org.anddev.andengine.engine.camera.hud.controls.AnalogOnScreenControl.IAnalogOnScreenControlListener;
//import org.anddev.andengine.engine.options.EngineOptions;
//import org.anddev.andengine.engine.options.EngineOptions.ScreenOrientation;
//import org.anddev.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
//import org.anddev.andengine.entity.layer.ILayer;
//import org.anddev.andengine.entity.primitive.Rectangle;
//import org.anddev.andengine.entity.scene.Scene;
//import org.anddev.andengine.entity.scene.Scene.IOnAreaTouchListener;
//import org.anddev.andengine.entity.scene.Scene.IOnSceneTouchListener;
//import org.anddev.andengine.entity.scene.Scene.ITouchArea;
//import org.anddev.andengine.entity.scene.background.ColorBackground;
//import org.anddev.andengine.entity.scene.menu.MenuScene;
//import org.anddev.andengine.entity.scene.menu.MenuScene.IOnMenuItemClickListener;
//import org.anddev.andengine.entity.scene.menu.item.ColoredTextMenuItem;
//import org.anddev.andengine.entity.scene.menu.item.IMenuItem;
//import org.anddev.andengine.entity.shape.Shape;
//import org.anddev.andengine.entity.sprite.AnimatedSprite;
//import org.anddev.andengine.entity.sprite.Sprite;
//import org.anddev.andengine.entity.sprite.TiledSprite;
//import org.anddev.andengine.entity.util.FPSLogger;
//import org.anddev.andengine.extension.physics.box2d.FixedStepPhysicsWorld;
//import org.anddev.andengine.extension.physics.box2d.PhysicsConnector;
//import org.anddev.andengine.extension.physics.box2d.PhysicsFactory;
//import org.anddev.andengine.extension.physics.box2d.PhysicsWorld;
//import org.anddev.andengine.input.touch.TouchEvent;
//import org.anddev.andengine.opengl.font.Font;
//import org.anddev.andengine.opengl.font.FontFactory;
//import org.anddev.andengine.opengl.texture.Texture;
//import org.anddev.andengine.opengl.texture.TextureOptions;
//import org.anddev.andengine.opengl.texture.region.TextureRegion;
//import org.anddev.andengine.opengl.texture.region.TextureRegionFactory;
//import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;
//import org.anddev.andengine.sensor.accelerometer.AccelerometerData;
//import org.anddev.andengine.sensor.accelerometer.IAccelerometerListener;
//import org.anddev.andengine.ui.activity.BaseGameActivity;
//import org.anddev.andengine.util.MathUtils;
//
//import android.graphics.Color;
//import android.view.KeyEvent;
//import android.view.MotionEvent;
//
//import com.badlogic.gdx.math.Vector2;
//import com.badlogic.gdx.physics.box2d.Body;
//import com.badlogic.gdx.physics.box2d.FixtureDef;
//import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
//
///**
// * @author Nicolas Gramlich
// * @since 22:43:20 - 15.07.2010
// */
//public class Bull extends BaseGameActivity implements IAccelerometerListener,
//		IOnSceneTouchListener, IOnAreaTouchListener,IOnMenuItemClickListener {
//	// ===========================================================
//	// Constants
//	// ===========================================================
//
//	private static final int RACETRACK_WIDTH = 64;
//
//	private static final int OBSTACLE_SIZE = 16;
//	private static final int CAR_SIZE = 16;
//
//	private static final int CAMERA_WIDTH = RACETRACK_WIDTH * 5;
//	private static final int CAMERA_HEIGHT = RACETRACK_WIDTH * 3;
//
//	private static final int LAYER_RACETRACK = 0;
//	private static final int LAYER_BORDERS = LAYER_RACETRACK + 1;
//	private static final int LAYER_CARS = LAYER_BORDERS + 1;
//	private static final int LAYER_OBSTACLES = LAYER_CARS + 1;
//	protected MenuScene mMenuScene;
//	
//	Vector2 prevPosition = new Vector2(0, 0);
//	final Scene scene = new Scene(4);
//	
//	private Texture mFontTexture;
//	private Font mFont;
//	
//	protected static final int MENU_RESET = 0;
//	protected static final int MENU_QUIT = MENU_RESET + 1;
//	
//	// int i = 2;
//	int boxCount = 0;
//	int spriteCount = 0;
//	private String selectedObstacle = "SMILEY";// box
//
//	Sprite box;
//	Sprite smileySprite;
//	Sprite track;
//	// ===========================================================
//	// Fields
//	// ===========================================================
//	// private Camera mCamera;
//	
//	private ArrayList footPrints;
//	
//	private Texture footPrintsTexture;
//	private TextureRegion footPrintsTextureRegion;
//	
//	private Rectangle rectangle;
//
//	private BoundCamera boundCamera;
//
//	private PhysicsWorld mPhysicsWorld;
//
//	private Texture mVehiclesTexture;
//	private TiledTextureRegion mVehiclesTextureRegion;
//
//	private Texture mBoxTexture;
//	private TextureRegion mBoxTextureRegion;
//
//	private Texture mRacetrackTexture;
//	private TextureRegion mRacetrackStraightTextureRegion;
//	// private TextureRegion mRacetrackCurveTextureRegion;
//
//	private Texture mOnScreenControlTexture;
//	private TextureRegion mOnScreenControlBaseTextureRegion;
//	private TextureRegion mOnScreenControlKnobTextureRegion;
//
//	private Texture selectObstacle;
//	private TextureRegion selectObstacleTextureRegion;
//
//	private Texture smiley;
//	private TextureRegion smileyTextRegion;
//
//	Body mCarBody;
//	private TiledSprite mCar;
//
//	private Body[] boxes = new Body[24];
//	private Body[] sprites = new Body[24];
//
//	public static final FixtureDef WALL_FIXTURE_DEF = PhysicsFactory
//			.createFixtureDef(0, 0.5f, 0.5f, false, /* CATEGORYBIT_WALL */
//					(short) 1, /* MASKBITS_WALL */(short) 7, (short) 0);
//
//	@Override
//	public Engine onLoadEngine() {
//		// this.mCamera = new Camera(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT);
//
//		this.boundCamera = new BoundCamera(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT);
//		this.boundCamera.setBounds(0, 30 * RACETRACK_WIDTH, 0,
//				30 * RACETRACK_WIDTH);
//		this.boundCamera.setBoundsEnabled(true);
//
//		return new Engine(new EngineOptions(true, ScreenOrientation.LANDSCAPE,
//				new RatioResolutionPolicy(CAMERA_WIDTH, CAMERA_HEIGHT),
//				this.boundCamera));
//	}
//
//	@Override
//	public void onLoadResources() {
//		TextureRegionFactory.setAssetBasePath("gfx/");
//
//		
//		this.mFontTexture = new Texture(256, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
//
//		FontFactory.setAssetBasePath("font/");
//		this.mFont = FontFactory.createFromAsset(this.mFontTexture, this, "Plok.ttf", 48, true, Color.WHITE);
//		
//		
//		this.mVehiclesTexture = new Texture(128, 16,
//				TextureOptions.BILINEAR_PREMULTIPLYALPHA);
//		this.mVehiclesTextureRegion = TextureRegionFactory
//				.createTiledFromAsset(this.mVehiclesTexture, this,
//						"vehicles.png", 0, 0, 6, 1);
//
//		this.mRacetrackTexture = new Texture(1024, 1024,
//				TextureOptions.REPEATING_BILINEAR_PREMULTIPLYALPHA);
//		this.mRacetrackStraightTextureRegion = TextureRegionFactory
//				.createFromAsset(this.mRacetrackTexture, this,
//						"racetrack_straight.png", 0, 0);
//		// this.mRacetrackCurveTextureRegion =
//		// TextureRegionFactory.createFromAsset(this.mRacetrackTexture, this,
//		// "racetrack_curve.png", 0, 128);
//
//		this.mOnScreenControlTexture = new Texture(256, 128,
//				TextureOptions.BILINEAR_PREMULTIPLYALPHA);
//		this.mOnScreenControlBaseTextureRegion = TextureRegionFactory
//				.createFromAsset(this.mOnScreenControlTexture, this,
//						"onscreen_control_base.png", 0, 0);
//		this.mOnScreenControlKnobTextureRegion = TextureRegionFactory
//				.createFromAsset(this.mOnScreenControlTexture, this,
//						"onscreen_control_knob.png", 128, 0);
//
//		this.mBoxTexture = new Texture(32, 32,
//				TextureOptions.BILINEAR_PREMULTIPLYALPHA);
//		this.mBoxTextureRegion = TextureRegionFactory.createFromAsset(
//				this.mBoxTexture, this, "box.png", 0, 0);
//
//		this.selectObstacle = new Texture(32, 32,
//				TextureOptions.REPEATING_BILINEAR_PREMULTIPLYALPHA);
//		this.selectObstacleTextureRegion = TextureRegionFactory
//				.createFromAsset(selectObstacle, this, "background_grass.png",
//						0, 0);
//
//		this.smiley = new Texture(128, 256,
//				TextureOptions.REPEATING_BILINEAR_PREMULTIPLYALPHA);
//		this.smileyTextRegion = TextureRegionFactory.createFromAsset(
//				this.smiley, this, "face_box.png", 0, 0);
//
//		this.mEngine.getTextureManager().loadTextures(this.mVehiclesTexture,
//				this.mRacetrackTexture, this.mOnScreenControlTexture,
//				this.mBoxTexture, this.selectObstacle, this.smiley);
//	}
//
//	@Override
//	public Scene onLoadScene() {
//
//		
//		this.mMenuScene = this.createMenuScene();
//
//		this.mEngine.registerUpdateHandler(new FPSLogger());
//
//		// boundCamera.setChaseShape(mCar);
//
//		
//		scene.setBackground(new ColorBackground(0, 0, 0));
//
//		this.mPhysicsWorld = new FixedStepPhysicsWorld(30, new Vector2(0, 0),
//				false, 8, 1);
//		// mPhysicsWorld.setGravity(new Vector2(1000, 1000));
//		// mPhysicsWorld.setVelocityIterations(0);
//
//		this.initRacetrack(scene);
//		// this.initRacetrackBorders(scene);
//		this.initCar(scene);
//
//		addInitialVelocityToCar();
//		this.initObstacles(scene);
//		this.initOnScreenControls(scene);
//
//		initSelectObstacle(scene);
//
//		
//		
//		final Shape ground = new Rectangle( 8 * RACETRACK_WIDTH - 2,0, 2, 3 *
//				 RACETRACK_WIDTH - 2);
//		ground.setColor(0, 1, 0);
//		 final Shape roof = new Rectangle(5, 0, 5, 3 * RACETRACK_WIDTH - 2);
//		 final Shape left = new Rectangle(0, 3 * RACETRACK_WIDTH - 2,  8 * RACETRACK_WIDTH - 2,2);
//		 final Shape right = new Rectangle(0,0, 8 * RACETRACK_WIDTH - 2, 0);
//		
//		 PhysicsFactory.createBoxBody(this.mPhysicsWorld, ground,
//		 BodyType.StaticBody, WALL_FIXTURE_DEF);
//		 PhysicsFactory.createBoxBody(this.mPhysicsWorld, roof,
//		 BodyType.StaticBody, WALL_FIXTURE_DEF);
//		 PhysicsFactory.createBoxBody(this.mPhysicsWorld, left,
//		 BodyType.StaticBody, WALL_FIXTURE_DEF);
//		 PhysicsFactory.createBoxBody(this.mPhysicsWorld, right,
//		 BodyType.StaticBody, WALL_FIXTURE_DEF);
//		
//		 scene.getBottomLayer().addEntity(ground);
//		 scene.getBottomLayer().addEntity(roof);
//		 scene.getBottomLayer().addEntity(left);
//		 scene.getBottomLayer().addEntity(right);
//
//		scene.registerUpdateHandler(this.mPhysicsWorld);
//		// scene.setOnSceneTouchListener(this);
//
//		// scene.getLayer(LAYER_RACETRACK).registerTouchArea(t)
//
//		return scene;
//	}
//	protected MenuScene createMenuScene() {
//		final MenuScene menuScene = new MenuScene(this.boundCamera);
//
//		final ColoredTextMenuItem resetMenuItem = new ColoredTextMenuItem(MENU_RESET, this.mFont, "RESET", 1.0f,0.0f,0.0f, 0.0f,0.0f,0.0f);
//		resetMenuItem.setBlendFunction(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
//		menuScene.addMenuItem(resetMenuItem);
//		
//		final ColoredTextMenuItem quitMenuItem = new ColoredTextMenuItem(MENU_QUIT, this.mFont, "QUIT", 1.0f,0.0f,0.0f, 0.0f,0.0f,0.0f);
//		quitMenuItem.setBlendFunction(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
//		menuScene.addMenuItem(quitMenuItem);
//		
//		menuScene.buildAnimations();
//
//		menuScene.setBackgroundEnabled(false);
//
//		menuScene.setOnMenuItemClickListener(this);
//		return menuScene;
//	}
//	private void initSelectObstacle(Scene scene) {
//		final TextureRegion selectObstacleTextureRegion = this.selectObstacleTextureRegion
//				.clone();
//		selectObstacleTextureRegion
//				.setWidth(3 * this.mRacetrackStraightTextureRegion.getWidth());
//		smileySprite = new Sprite(0, 0, 20, 3 * RACETRACK_WIDTH,
//				selectObstacleTextureRegion);
//		scene.getLayer(LAYER_OBSTACLES).addEntity(smileySprite);
//		final TextureRegion smileyTextRegionn = this.smileyTextRegion.clone();
//		smileyTextRegionn.setWidth(30);
//
//		Sprite smiley = new Sprite(5, 35, 15, 15, smileyTextRegionn) {
//			@Override
//			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,
//					float pTouchAreaLocalX, float pTouchAreaLocalY) {
//
//				selectedObstacle = "SMILEY";
//				System.out.println("smiley");
//				return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX,
//						pTouchAreaLocalY);
//			}
//		};
//
//		scene.getLayer(LAYER_OBSTACLES).addEntity(smiley);
//		scene.getLayer(LAYER_OBSTACLES).registerTouchArea(smiley);
//
//		box = new Sprite(5, 65, 15, 15, mBoxTextureRegion.clone()) {
//			@Override
//			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,
//					float pTouchAreaLocalX, float pTouchAreaLocalY) {
//
//				selectedObstacle = "BOX";
//				System.out.println("BOX");
//				return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX,
//						pTouchAreaLocalY);
//			}
//		};
//
//		scene.getLayer(LAYER_OBSTACLES).addEntity(box);
//		scene.getLayer(LAYER_OBSTACLES).registerTouchArea(box);
//
//	}
//
//	private void addInitialVelocityToCar() {
////		mCarBody.setAngularVelocity(20);
//		 mCarBody.setLinearVelocity(new Vector2(1, 1));
//
////		mCarBody.applyForce(new Vector2(10, 10), mCarBody.getWorldCenter());
//
//		// MassData mass = new MassData();
//		// mass.mass = 10000000;
//		// mCarBody.setMassData(mass);
//
//	}
//
//	@Override
//	public void onLoadComplete() {
//
//	}
//
//	// ===========================================================
//	// Methods
//	// ===========================================================
//
//	private void initOnScreenControls(final Scene pScene) {
//		final AnalogOnScreenControl analogOnScreenControl = new AnalogOnScreenControl(
//				0, CAMERA_HEIGHT
//						- this.mOnScreenControlBaseTextureRegion.getHeight(),
//				this.boundCamera, this.mOnScreenControlBaseTextureRegion,
//				this.mOnScreenControlKnobTextureRegion, 0.1f,
//				new IAnalogOnScreenControlListener() {
//					private Vector2 mVelocityTemp = new Vector2(2, 0);
//
//					@Override
//					public void onControlChange(
//							final BaseOnScreenControl pBaseOnScreenControl,
//							final float pValueX, final float pValueY) {
//						// mCarBody.applyForce(new Vector2(2, 2),
//						// mCarBody.getWorldCenter());
//						this.mVelocityTemp.set(pValueX * 5, pValueY * 5);
//
//						for (int i = 0; i < boxCount; i++) {
//							/* System.out.println("car to obstacle distance "+ */
//							float dist = mCarBody.getPosition().dst(boxes[i].getPosition());
//							System.out.println(dist);
//							if (dist < 2) {
////								mCarBody.applyAngularImpulse(dist*50);
//								mCarBody.applyForce((mCarBody.getPosition()
//										.sub(boxes[i].getPosition())).mul(3),
//										mCarBody.getWorldCenter());
////								mCarBody.applyTorque(10);
//							}
//						}
//
//						for (int i = 0; i < spriteCount; i++) {
//							/* System.out.println("car to obstacle distance "+ */
//							float angle = mCarBody.getAngle();
//							float dist = mCarBody.getPosition().dst(sprites[i].getPosition());
//							if (dist < 2) {
////								mCarBody.applyAngularImpulse(dist*50);
//
//								mCarBody.applyForce(
//										(mCarBody.getPosition().sub(sprites[i]
//												.getPosition())).mul(-3),
//										mCarBody.getWorldCenter());
////								mCarBody.applyTorque(10);
//							}
//						}
//
//						final Body carBody = Bull.this.mCarBody;
//						carBody.setLinearVelocity(this.mVelocityTemp);
//
//						final float rotationInRad = (float) Math.atan2(
//								-pValueX, pValueY);
//						carBody.setTransform(carBody.getWorldCenter(),
//								rotationInRad);
//
//						Bull.this.mCar.setRotation(MathUtils
//								.radToDeg(rotationInRad));
//						
//						
//						drawFootPrint(pScene);
//					}
//
//					@Override
//					public void onControlClick(
//							AnalogOnScreenControl pAnalogOnScreenControl) {
//						/* Nothing. */
//					}
//				});
//		analogOnScreenControl.getControlBase().setBlendFunction(
//				GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
//		analogOnScreenControl.getControlBase().setAlpha(0.5f);
//		analogOnScreenControl.getControlBase().setScaleCenter(0, 128);
//		analogOnScreenControl.getControlBase().setScale(0.75f);
//		analogOnScreenControl.getControlKnob().setScale(0.75f);
//		analogOnScreenControl.refreshControlKnobPosition();
//
//		pScene.setChildScene(analogOnScreenControl);
//	}
//	
//	
//	public void drawFootPrint(Scene pScene) {
//		Vector2 currentPosition = mCarBody.getPosition();
//		
//		if(currentPosition.x - prevPosition.x >  1){
//			
//			Rectangle rect = new Rectangle(currentPosition.x, currentPosition.y, 10, 10);
//			float[] coordinatess = rect.convertLocalToSceneCoordinates(currentPosition.x, currentPosition.y);
//			
//			
//			rect.setPosition(mCar);
////			rect.setPosition(coordinatess[0], coordinatess[1]);
//			System.out.println("1="+coordinatess[0] + "    " + coordinatess[1]);
//			Rectangle rect2 = new Rectangle(currentPosition.x, currentPosition.y, 10, 10);
//
//			float[] coord2 = rect2.convertSceneToLocalCoordinates(currentPosition.x, currentPosition.y);
//			System.out.println("1="+coord2[0] + "    " + coord2[1]);
////			rect.setPosition(coord2[0], coord2[0]);			
//			
//			rect2.setColor(0, 1, 0);
//			prevPosition.x = currentPosition.x;
//			prevPosition.y = currentPosition.y;
//			pScene.getBottomLayer().addEntity(rect);
////			pScene.getBottomLayer().addEntity(rect2);
//		}
//		
//		
//		
//	}
//
//	private void initCar(final Scene pScene) {
//		this.mCar = new TiledSprite(20, 20, CAR_SIZE, CAR_SIZE,
//				this.mVehiclesTextureRegion);
//		this.mCar.setUpdatePhysics(false);
//		this.mCar.setCurrentTileIndex(0);
//		mCar.setVelocity(100.0f, 100.0f);
//		final FixtureDef carFixtureDef = PhysicsFactory.createFixtureDef(1,
//				0.5f, 0.5f);
//		this.mCarBody = PhysicsFactory.createBoxBody(this.mPhysicsWorld,
//				this.mCar, BodyType.DynamicBody, carFixtureDef);
//mCarBody.setLinearVelocity(new Vector2(100f, 100f	));
//		
//		this.mPhysicsWorld.registerPhysicsConnector(new PhysicsConnector(
//				this.mCar, this.mCarBody, true, false, true, false));
//
//		pScene.getLayer(LAYER_CARS).addEntity(this.mCar);
//
//		boundCamera.setChaseShape(mCar);
//	}
//
//	private void initObstacles(final Scene pScene) {
//		// Body boxBody = addObstacle(pScene, CAMERA_WIDTH / 2,
//		// RACETRACK_WIDTH / 2);
//		// boxes[0] = boxBody;
//		// boxes[1] = addObstacle(pScene, CAMERA_WIDTH / 2, RACETRACK_WIDTH /
//		// 2);
//		// boxes[2]= addObstacle(pScene, CAMERA_WIDTH / 2, CAMERA_HEIGHT -
//		// RACETRACK_WIDTH / 2);
//		// boxes[3]= addObstacle(pScene, CAMERA_WIDTH / 2, CAMERA_HEIGHT -
//		// RACETRACK_WIDTH / 2);
//	}
//
//	private Body addObstacle(final Scene pScene, final float pX, final float pY) {
//
//		box = new Sprite(pX, pY, OBSTACLE_SIZE, OBSTACLE_SIZE,
//				this.mBoxTextureRegion);
//		box.setUpdatePhysics(false);
//
//		final FixtureDef boxFixtureDef = PhysicsFactory.createFixtureDef(10,
//				0.5f, 0.5f);
//		final Body boxBody = PhysicsFactory.createBoxBody(this.mPhysicsWorld,
//				box, BodyType.DynamicBody, boxFixtureDef);
//
//		boxBody.setLinearDamping(10);
//		boxBody.setAngularDamping(10);
//		this.mPhysicsWorld.registerPhysicsConnector(new PhysicsConnector(box,
//				boxBody, true, true, false, false));
//
//		pScene.getLayer(LAYER_OBSTACLES).addEntity(box);
//		return boxBody;
//	}
//
//	private Body addSprite(Scene pScene, float pX, float pY) {
//
//		smileySprite = new Sprite(pX, pY, OBSTACLE_SIZE, OBSTACLE_SIZE,
//				this.smileyTextRegion);
//		smileySprite.setUpdatePhysics(false);
//
//		final FixtureDef smileyFixtureDef = PhysicsFactory.createFixtureDef(10,
//				0.5f, 0.5f);
//		final Body smileyBody = PhysicsFactory.createBoxBody(
//				this.mPhysicsWorld, smileySprite, BodyType.DynamicBody,
//				smileyFixtureDef);
//		smileyBody.setLinearDamping(10);
//		smileyBody.setAngularDamping(10);
//		this.mPhysicsWorld.registerPhysicsConnector(new PhysicsConnector(
//				smileySprite, smileyBody, true, true, false, false));
//
//		pScene.getLayer(LAYER_OBSTACLES).addEntity(smileySprite);
//		return smileyBody;
//
//	}
//
//	@Override
//	public boolean onAreaTouched(final TouchEvent pSceneTouchEvent,
//			final ITouchArea pTouchArea, final float pTouchAreaLocalX,
//			final float pTouchAreaLocalY) {
//		if (pSceneTouchEvent.getAction() == MotionEvent.ACTION_DOWN) {
//			this.mPhysicsWorld.postRunnable(new Runnable() {
//				@Override
//				public void run() {
//					final AnimatedSprite face = (AnimatedSprite) pTouchArea;
//
//				}
//			});
//			return true;
//		}
//
//		return false;
//	}
//
//	private void initRacetrack(final Scene pScene) {
//		final ILayer racetrackLayer = pScene.getLayer(LAYER_RACETRACK);
//
//		/* Straights. */
//		{
//			final TextureRegion racetrackHorizontalStraightTextureRegion = this.mRacetrackStraightTextureRegion
//					.clone();
//
//			racetrackHorizontalStraightTextureRegion
//					.setWidth(1 * this.mRacetrackStraightTextureRegion
//							.getWidth());
//
//			// racetrackHorizontalStraightTextureRegion
//			// .setHeight(10000);
//
//			// final TextureRegion racetrackVerticalStraightTextureRegion =
//			// this.mRacetrackStraightTextureRegion;
//
//			track = new Sprite(0, 0, 8 * RACETRACK_WIDTH, 3 * RACETRACK_WIDTH,
//					racetrackHorizontalStraightTextureRegion) {
//
//				@Override
//				public boolean onAreaTouched(TouchEvent pSceneTouchEvent,
//						float pTouchAreaLocalX, float pTouchAreaLocalY) {
//
//					Bull.this.addObject(pSceneTouchEvent.getX(),
//							pSceneTouchEvent.getY());
//
//					return super.onAreaTouched(pSceneTouchEvent,
//							pTouchAreaLocalX, pTouchAreaLocalY);
//				}
//			};
//
//			racetrackLayer.addEntity(track);
//			racetrackLayer.registerTouchArea(track);
//			// racetrackLayer.addEntity(new Sprite(0, RACETRACK_WIDTH,
//			// 5 * RACETRACK_WIDTH, RACETRACK_WIDTH,
//			// racetrackHorizontalStraightTextureRegion));
//			// racetrackLayer.addEntity(new Sprite(0, 2 * RACETRACK_WIDTH,
//			// 5 * RACETRACK_WIDTH, RACETRACK_WIDTH,
//			// racetrackHorizontalStraightTextureRegion));
//			// racetrackLayer.addEntity(new Sprite(RACETRACK_WIDTH,
//			// CAMERA_HEIGHT - RACETRACK_WIDTH, 3 * RACETRACK_WIDTH,
//			// RACETRACK_WIDTH, racetrackHorizontalStraightTextureRegion));
//
//			// /* Left Straight */
//			// final Sprite leftVerticalStraight = new Sprite(0,
//			// RACETRACK_WIDTH, RACETRACK_WIDTH, RACETRACK_WIDTH,
//			// racetrackVerticalStraightTextureRegion);
//			// leftVerticalStraight.setRotation(90);
//			// racetrackLayer.addEntity(leftVerticalStraight);
//			// /* Right Straight */
//			// final Sprite rightVerticalStraight = new Sprite(CAMERA_WIDTH -
//			// RACETRACK_WIDTH, RACETRACK_WIDTH, RACETRACK_WIDTH,
//			// RACETRACK_WIDTH, racetrackVerticalStraightTextureRegion);
//			// rightVerticalStraight.setRotation(90);
//			// racetrackLayer.addEntity(rightVerticalStraight);
//		}
//
//		/* Edges */
//		// {
//		// final TextureRegion racetrackCurveTextureRegion =
//		// this.mRacetrackCurveTextureRegion;
//		//
//		// /* Upper Left */
//		// final Sprite upperLeftCurve = new Sprite(0, 0, RACETRACK_WIDTH,
//		// RACETRACK_WIDTH, racetrackCurveTextureRegion);
//		// upperLeftCurve.setRotation(90);
//		// racetrackLayer.addEntity(upperLeftCurve);
//		//
//		// /* Upper Right */
//		// final Sprite upperRightCurve = new Sprite(CAMERA_WIDTH -
//		// RACETRACK_WIDTH, 0, RACETRACK_WIDTH, RACETRACK_WIDTH,
//		// racetrackCurveTextureRegion);
//		// upperRightCurve.setRotation(180);
//		// racetrackLayer.addEntity(upperRightCurve);
//		//
//		// /* Lower Right */
//		// final Sprite lowerRightCurve = new Sprite(CAMERA_WIDTH -
//		// RACETRACK_WIDTH, CAMERA_HEIGHT - RACETRACK_WIDTH, RACETRACK_WIDTH,
//		// RACETRACK_WIDTH, racetrackCurveTextureRegion);
//		// lowerRightCurve.setRotation(270);
//		// racetrackLayer.addEntity(lowerRightCurve);
//		//
//		// /* Lower Left */
//		// final Sprite lowerLeftCurve = new Sprite(0, CAMERA_HEIGHT -
//		// RACETRACK_WIDTH, RACETRACK_WIDTH, RACETRACK_WIDTH,
//		// racetrackCurveTextureRegion);
//		// racetrackLayer.addEntity(lowerLeftCurve);
//		// }
//	}
//
//	private void initRacetrackBorders(final Scene pScene) {
//		final Shape bottomOuter = new Rectangle(0, CAMERA_HEIGHT - 2,
//				CAMERA_WIDTH, 2);
//		final Shape topOuter = new Rectangle(0, 0, CAMERA_WIDTH, 2);
//		final Shape leftOuter = new Rectangle(0, 0, 2, CAMERA_HEIGHT);
//		final Shape rightOuter = new Rectangle(CAMERA_WIDTH - 2, 0, 2,
//				CAMERA_HEIGHT);
//
//		final Shape bottomInner = new Rectangle(RACETRACK_WIDTH, CAMERA_HEIGHT
//				- 2 - RACETRACK_WIDTH, CAMERA_WIDTH - 2 * RACETRACK_WIDTH, 2);
//		final Shape topInner = new Rectangle(RACETRACK_WIDTH, RACETRACK_WIDTH,
//				CAMERA_WIDTH - 2 * RACETRACK_WIDTH, 2);
//		final Shape leftInner = new Rectangle(RACETRACK_WIDTH, RACETRACK_WIDTH,
//				2, CAMERA_HEIGHT - 2 * RACETRACK_WIDTH);
//		final Shape rightInner = new Rectangle(CAMERA_WIDTH - 2
//				- RACETRACK_WIDTH, RACETRACK_WIDTH, 2, CAMERA_HEIGHT - 2
//				* RACETRACK_WIDTH);
//
//		final FixtureDef wallFixtureDef = PhysicsFactory.createFixtureDef(0,
//				0.5f, 0.5f);
//		PhysicsFactory.createBoxBody(this.mPhysicsWorld, bottomOuter,
//				BodyType.StaticBody, wallFixtureDef);
//		PhysicsFactory.createBoxBody(this.mPhysicsWorld, topOuter,
//				BodyType.StaticBody, wallFixtureDef);
//		PhysicsFactory.createBoxBody(this.mPhysicsWorld, leftOuter,
//				BodyType.StaticBody, wallFixtureDef);
//		PhysicsFactory.createBoxBody(this.mPhysicsWorld, rightOuter,
//				BodyType.StaticBody, wallFixtureDef);
//
//		PhysicsFactory.createBoxBody(this.mPhysicsWorld, bottomInner,
//				BodyType.StaticBody, wallFixtureDef);
//		PhysicsFactory.createBoxBody(this.mPhysicsWorld, topInner,
//				BodyType.StaticBody, wallFixtureDef);
//		PhysicsFactory.createBoxBody(this.mPhysicsWorld, leftInner,
//				BodyType.StaticBody, wallFixtureDef);
//		PhysicsFactory.createBoxBody(this.mPhysicsWorld, rightInner,
//				BodyType.StaticBody, wallFixtureDef);
//
//		final ILayer bottomLayer = pScene.getLayer(LAYER_BORDERS);
//		bottomLayer.addEntity(bottomOuter);
//		bottomLayer.addEntity(topOuter);
//		bottomLayer.addEntity(leftOuter);
//		bottomLayer.addEntity(rightOuter);
//
//		bottomLayer.addEntity(bottomInner);
//		bottomLayer.addEntity(topInner);
//		bottomLayer.addEntity(leftInner);
//		bottomLayer.addEntity(rightInner);
//	}
//
//	@Override
//	public boolean onSceneTouchEvent(final Scene pScene,
//			final TouchEvent pSceneTouchEvent) {
//		// if (this.mPhysicsWorld != null) {
//		// if (pSceneTouchEvent.getAction() == MotionEvent.ACTION_DOWN) {
//		//
//		// this.runOnUpdateThread(new Runnable() {
//		// @Override
//		// public void run() {
//		// Bull.this.addObject(pSceneTouchEvent.getX(),
//		// pSceneTouchEvent.getY());
//		// }
//		// });
//		// return true;
//		// }
//		// }
//		return false;
//	}
//
//	private void addObject(final float pX, final float pY) {
//		final Scene scene = this.mEngine.getScene();
//
//		if (selectedObstacle.equals("BOX")) {
//			boxes[boxCount++] = addObstacle(scene, pX, pY);
//
//		} else {
//			sprites[spriteCount++] = addSprite(scene, pX, pY);
//		}
//
//		addObstacle(scene, 1020, 1020);
//	}
//
//	@Override
//	public void onAccelerometerChanged(AccelerometerData pAccelerometerData) {
//		// TODO Auto-generated method stub
//
//	}
//	
//	
//	
//	
//	
//	
//	
//	
//	
//	@Override
//	public boolean onKeyDown(final int pKeyCode, final KeyEvent pEvent) {
//		if(pKeyCode == KeyEvent.KEYCODE_MENU && pEvent.getAction() == KeyEvent.ACTION_DOWN) {
//			if(this.scene.hasChildScene()) {
//				/* Remove the menu and reset it. */
//				this.scene.back();
//			} else {
//				/* Attach the menu. */
//				this.scene.setChildScene(this.mMenuScene, false, true, true);
//			}
//			return true;
//		} else {
//			return super.onKeyDown(pKeyCode, pEvent);
//		}
//	}
//
//	@Override
//	public boolean onMenuItemClicked(final MenuScene pMenuScene, final IMenuItem pMenuItem, final float pMenuItemLocalX, final float pMenuItemLocalY) {
//		switch(pMenuItem.getID()) {
//			case MENU_RESET:
//				/* Restart the animation. */
//				this.scene.reset();
//
//				/* Remove the menu and reset it. */
//				this.scene.clearChildScene();
//				this.mMenuScene.reset();
//				return true;
//			case MENU_QUIT:
//				/* End Activity. */
//				this.finish();
//				return true;
//			default:
//				return false;
//		}
//	
//	
//	
//	
//	}
//	
//	
//	
//	
//	
//	
//	
//	
//	
//}
